#ifndef BOARD_HAS_PSRAM
#error "Please enable PSRAM !!!"
#endif

#include <Arduino.h>
#include <esp_task_wdt.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <ESP32Time.h>
#include "epd_driver.h"
#include "7segment120.h" // convertida de https://www.keshikan.net/fonts-e.html, solo caracteres 0-9 y : 
#include <Wire.h>
#include <SPI.h>
#include <WiFi.h>
#include "passwifi.h"
#include "driver/adc.h"
#include "esp_sleep.h"

#define BATT_PIN            36
ESP32Time rtc;
static const char *mn[12] = {"Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dec"};
static const char *wd[7] = {"Lun","Mar","Mié","Jue","Vie","Sáb","Dom"};


static const char* ntpServer = "pool.ntp.org";
static const uint32_t gmtOffset_sec = 1 * 3600;
static const uint32_t daylightOffset_sec = 3600;

static char RTC_DATA_ATTR hora_act[5];

// Adaptado de www.mischianti.org  
void disableWifi(){
    // adc_power_off();
    WiFi.disconnect(true);
    WiFi.mode(WIFI_OFF);
}

void enableWifi(){
    // adc_power_on();
    WiFi.disconnect(false);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(1000);
    } 
}

/* Al despertar se ejecuta esta rutina antes de arrancar el idf. */
// static void RTC_IRAM_ATTR  esp_wake_deep_sleep(void)
// {
//}

void setup()
{
    char buf[128];
    int hour;
    int minute;
    Serial.begin(115200);
    epd_init();
    // No volver a pedir la hora si despertamos de deep sleep
    if (esp_sleep_get_wakeup_cause() == ESP_SLEEP_WAKEUP_UNDEFINED) 
    {
  
  /*
      // Corregir el valor de referencia del ADC
      esp_adc_cal_characteristics_t adc_chars;
      esp_adc_cal_value_t val_type = esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_11, ADC_WIDTH_BIT_12, 1100, &adc_chars);
      if (val_type == ESP_ADC_CAL_VAL_EFUSE_VREF) {
          Serial.printf("eFuse Vref:%u mV", adc_chars.vref);
          vref = adc_chars.vref;
      }
  */
  /* No estoy usando framebuffer
      framebuffer = (uint8_t *)ps_calloc(sizeof(uint8_t), EPD_WIDTH * EPD_HEIGHT / 2);
      if (!framebuffer) {
          Serial.println("alloc memory failed !!!");
          while (1);
      }
      memset(framebuffer, 0xFF, EPD_WIDTH * EPD_HEIGHT / 2);
  */
      epd_poweron();
      epd_clear();
      strcpy(hora_act,"00:00");
      enableWifi();
      configTime( gmtOffset_sec, daylightOffset_sec, ntpServer);
      sprintf( hora_act, "%02d:%02d", rtc.getHour(true), rtc.getMinute());
      disableWifi();    
      epd_poweroff();
    } 
    else
    { // ¿ Funcionará sin wifi ?
      configTime( gmtOffset_sec, daylightOffset_sec, ntpServer);
    }
}

void loop()
{
    // When reading the battery voltage, POWER_EN must be turned on
    epd_poweron();
    // medirBateria();
    mostrarHora();
    epd_poweroff_all();
    //delay(30000);
    
    // Estas no se hacen automáticamente al entrar en deep sleep, 
    // se apagan a las bravas,tenerlo en cuenta
    // esp_bluedroid_disable()
    // esp_bt_controller_disable()
    // esp_wifi_stop()

    // El fabricante recomienda aislar este GPIO en las ESP-WROVER para reducir el consumo 
    // rtc_gpio_isolate(GPIO_NUM_12);
    ESP.deepSleep(30e6);
}

void mostrarHora()
{
    // Borrar la hora anterior
    int cursor_x = 90;
    int cursor_y = Sseg.advance_y;
    write_mode((GFXfont *)&Sseg,hora_act, &cursor_x,&cursor_y,NULL, WHITE_ON_WHITE,NULL);
    // Obtener nueva hora
    sprintf( hora_act, "%02d:%02d", rtc.getHour(true), rtc.getMinute());
    if (rtc.getMinute() % 10 == 0) {
        epd_clear();
    }
    // Escribir el nuevo valor
    cursor_x = 90;
    cursor_y = Sseg.advance_y;
    write_mode((GFXfont *)&Sseg,hora_act, &cursor_x,&cursor_y,NULL, BLACK_ON_WHITE,NULL);
  
}

void medirBateria()
{
    // adc_power_on();
    // uint16_t v = analogRead(BATT_PIN);
    // float battery_voltage = ((float)v / 4095.0) * 2.0 * 3.3 * (vref / 1000.0);
    // String voltage = "Voltaje batería: " + String(battery_voltage) + "V";
    // Serial.println(voltage);
    // adc_power_off();

}
